package LiveCrius

import (
	"context"
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/go-redis/redis/v8"
	CriusUtils "gitlab.com/RPGPN/crius-utils"
	cc "gitlab.com/RPGPN/criuscommander/v2"
	"gitlab.com/ponkey364/golesh-chat"
	"net/http"
	"time"
)

// glimesh | twitch
const SupportedPlatforms = 1<<1 | 1<<2

type Settings interface {
	CriusUtils.Settings
	GetRedisCache() *redis.Client
	GetRedisStore() *redis.Client
}

func GlimeshHasPermission(msg *goleshchat.ChatMessage) bool {
	return msg.User.ID == msg.Channel.Streamer.ID
}

func TwitchHasPermission(msg *twitch.PrivateMessage) bool {
	if _, ok := msg.User.Badges["moderator"]; ok {
		// they're a mod
		return true
	} else if _, ok := msg.User.Badges["broadcaster"]; ok {
		return true
	}

	return false
}

type APIFuncs interface {
	MakeTwitchReq(*http.Request) (*http.Response, error)
	MakeGlimeshReq(*http.Request, bool) (*http.Response, error)
	GlimeshGetChannelID(string) (*int, error)

	// Returns the val in the cache (as a *string, you may need to convert), true if a cache hit, an error
	CheckCache(cc.PlatformType, string, interface{}, *redis.Client) (*string, bool, error)
	AddToCache(cc.PlatformType, string, interface{}, interface{}, *redis.Client, time.Duration) error
}

// funcs to make getting from ctx nicer

func GetAPIFuncs(ctx context.Context) APIFuncs {
	return ctx.Value("API").(APIFuncs)
}

func GetSettings(ctx context.Context) Settings {
	return ctx.Value("settings").(Settings)
}
