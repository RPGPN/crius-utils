module gitlab.com/RPGPN/crius-utils

go 1.15

require (
	github.com/asaskevich/EventBus v0.0.0-20200907212545-49d423059eef
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/go-chi/chi/v5 v5.0.3
	github.com/go-redis/redis/v8 v8.6.0
	github.com/gorilla/csrf v1.7.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/rbcervilla/redisstore/v8 v8.0.0
	gitlab.com/RPGPN/criuscommander/v2 v2.2.6
	gitlab.com/ponkey364/golesh-chat v1.1.2
)
